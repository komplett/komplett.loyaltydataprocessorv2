$env:AZURE_STORAGE_ACCOUNT="#{Pulumi.AzureBlobStorageAccountName}"
$env:AZURE_STORAGE_KEY="#{Pulumi.AzureBlobStorageAccountKey}"
$env:PULUMI_CONFIG_PASSPHRASE="#{Pulumi.ConfigPassphrase}"

az account set -s #{Pulumi.DevAzureSubscription}
pulumi login azblob://pulumi-state
pulumi stack select #{Pulumi.StackName} -c --non-interactive
pulumi preview -s #{Pulumi.StackName} --cwd "bin\Debug\net8.0\win-x64"
