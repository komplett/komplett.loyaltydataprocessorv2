﻿namespace Komplett.LoyaltyDataProcessor.Infrastructure;

using Komplett.Infrastructure.Pulumi.Azure;
using Komplett.Infrastructure.Pulumi.Azure.Resources;
using Komplett.Infrastructure.Pulumi.Azure.Resources.ServiceBus;
using Pulumi;
using Pulumi.AzureNative.Resources;
using Pulumi.AzureNative.ServiceBus;
using Pulumi.AzureNative.Storage;
using Queue=Pulumi.AzureNative.ServiceBus.Queue;
using QueueArgs=Pulumi.AzureNative.ServiceBus.QueueArgs;

public static class AzureStackExtensions
{
    private const int MaxSubscriptionDeliveryCount = 3;

    public static Namespace CreateServiceBus(this ResourceGroup resourceGroup, NamingConventions names)
    {
        return new Namespace(names.ServiceBus.Namespace(), new()
        {
            NamespaceName = names.ServiceBus.Namespace(),
            ResourceGroupName = names.ResourceGroup,
            Location = resourceGroup.Location,
        });
    }

    public static Output<string> GetPrimaryConnectionString(this Namespace serviceBus, NamingConventions names)
    {
        var protectedOptionsThatEnsuresTheResourceCantBeDeleted = CustomResourceOptionsFactory.Create();
        var namespaceRule = serviceBus.AddAuthorizationRule(NamespaceAuthorizationRuleArgsFactory.Default(names.ServiceBus.AuthRule()), protectedOptionsThatEnsuresTheResourceCantBeDeleted);

        return namespaceRule.PrimaryConnectionString();
    }

    public static StorageAccount CreateStorageAccount(this ResourceGroup resourceGroup, NamingConventions names)
    {
        return new StorageAccount(names.StorageAccount, new StorageAccountArgs
        {
            AccountName = names.StorageAccount,
            ResourceGroupName = resourceGroup.Name,
            Location = resourceGroup.Location,
            Kind = Kind.StorageV2,
            Sku = new Pulumi.AzureNative.Storage.Inputs.SkuArgs
            {
                Name = Pulumi.AzureNative.Storage.SkuName.Standard_LRS
            }
        });
    }

    public static void CreateTable(this StorageAccount storageAccount, string tableName, string? resourceName = null) => _ = new Pulumi.Azure.Storage.Table(resourceName ?? tableName, new Pulumi.Azure.Storage.TableArgs
    {
        Name = tableName,
        StorageAccountName = storageAccount.Name,
    });

    public static void CreateContainer(this StorageAccount storageAccount, string containerName, string containerAccessType = "private") => _ = new Pulumi.Azure.Storage.Container(containerName, new Pulumi.Azure.Storage.ContainerArgs
    {
        Name = containerName,
        StorageAccountName = storageAccount.Name,
        ContainerAccessType = containerAccessType,
    });

    public static Output<Topic> CreteTopic(this Namespace serviceBus, string name, ResourceGroup resourceGroup)
    {
        return serviceBus.AddTopic(new TopicArgs()
        {
            TopicName = name,
            EnableBatchedOperations = true,
            SupportOrdering = true,
            ResourceGroupName = resourceGroup.Name
        });
    }

    public static Output<Queue> CreateQueue(this Namespace serviceBus, string name, ResourceGroup resourceGroup, bool requiresSession)
    {
        return serviceBus.AddQueue(new QueueArgs()
        {
            QueueName = name,
            EnableBatchedOperations = true,
            ResourceGroupName = resourceGroup.Name,
            RequiresSession = requiresSession,
        });
    }

    public static void AddSubscription(this Output<Topic> topic, string name, ResourceGroup resourceGroup, bool requiresSession = true)
    {
        _ = topic.AddSubscription(new SubscriptionArgs()
        {
            SubscriptionName = name,
            MaxDeliveryCount = MaxSubscriptionDeliveryCount,
            RequiresSession = requiresSession,
            DeadLetteringOnMessageExpiration = true,
            ResourceGroupName = resourceGroup.Name,
            EnableBatchedOperations = true
        });
    }

    public static Output<Subscription> AddSubscription2(this Output<GetTopicResult> topic, string subscriptioName, Output<string> resourceGroupName)
    {
       return topic.AddSubscription(new SubscriptionArgs()
        {
            SubscriptionName = subscriptioName,
            MaxDeliveryCount = MaxSubscriptionDeliveryCount,
            EnableBatchedOperations = true,
            RequiresSession = true,
            ResourceGroupName = resourceGroupName,
        });
    }

    public static Output<Subscription> AddSqlRule(this Output<Subscription> subscription, string ruleName, string ruleContent)
    {
        subscription.AddRule(new RuleArgs()
        {
            RuleName = ruleName,
            FilterType = FilterType.SqlFilter,
            SqlFilter = new Pulumi.AzureNative.ServiceBus.Inputs.SqlFilterArgs()
            {
                SqlExpression = ruleContent,
            },
        });

        return subscription;
    }

    public static NamingConventions GetNamespace(string environment, string projectName) => new(projectName, environment);

    public static Output<GetTopicResult> GetSubscriptionTopic(this NamingConventions namingConventions, string topicName)
    {
        var topic = GetTopic.Invoke(new GetTopicInvokeArgs()
        {
            NamespaceName = namingConventions.ServiceBus.Namespace(),
            ResourceGroupName = namingConventions.ResourceGroup,
            TopicName = topicName
        });

        return topic;
    }
}