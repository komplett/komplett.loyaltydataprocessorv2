namespace Komplett.LoyaltyDataProcessor.Infrastructure
{
    using System.Collections.Generic;
    using Komplett.Infrastructure.Pulumi.Azure;
    using Komplett.Infrastructure.Pulumi.Azure.Resources.StorageAccount;
    using Komplett.Infrastructure.Pulumi.Octopus.Deployment;
    using Pulumi;
    using Pulumi.AzureNative.Insights;
    using Pulumi.AzureNative.Resources;
    using Pulumi.AzureNative.Storage;
    using Pulumi.AzureNative.Web;
    using Pulumi.AzureNative.Web.Inputs;

    public class AzureStack
    {
        private readonly string _env;

        private readonly NamingConventions _names;
        private readonly NamingConventions _clubDomainNames;

        public AzureStack()
        {
            var config = new Pulumi.Config("app");
            _env = config.Require("Environment");
            _names = new NamingConventions("Komplett.LoyaltyDataProcessor", _env);
            _clubDomainNames = new NamingConventions("Komplett.Club", _env);

            CreateResources(config);
        }

        private void CreateResources(Config config)
        {
            var resourceGroup = new ResourceGroup(_names.ResourceGroup, new ResourceGroupArgs
            {
                ResourceGroupName = _names.ResourceGroup,
                Location = Locations.NorwayEast,
            });

            var storageAccount = resourceGroup.CreateStorageAccount(_names);
            storageAccount.CreateContainer("orders");
            storageAccount.CreateTable("LoyaltyPointMultiplierDates");

            ConfigureStorageAccountCors(storageAccount, resourceGroup);
            
            CreateClubResources();

            var serviceBus = resourceGroup.CreateServiceBus(_names);
            serviceBus.CreateQueue("add-points", resourceGroup, requiresSession: true);
            serviceBus.CreateQueue($"{_env}-user-benefits-messaging-queue", resourceGroup, requiresSession: false);
            serviceBus.CreateQueue("loyalty-program-add-points", resourceGroup, requiresSession: false);
            serviceBus.CreteTopic("return-benefits-order-canceled", resourceGroup)
                      .AddSubscription("Komplett.LoyaltyProgram.Api", resourceGroup);

            var serviceBusConnectionString = serviceBus.GetPrimaryConnectionString(_names);

            var appInsights = CreateAppInsights(resourceGroup);
            var appServicePlan = CreateAppServicePlan(resourceGroup);
            _ = CreateWebApp(config, resourceGroup, appServicePlan, serviceBusConnectionString, appInsights, storageAccount);

            var thisProjectVariableSet  = new OctopusVariablesMap
            {
                { "Azure.ResourceGroup", _names.ResourceGroup },
                { "Azure.FunctionApp", _names.FunctionApp },
                { "LoyaltyDataProcessor.StorageAccount.ConnectionString", storageAccount.PrimaryConnectionString().AsSecret() },
                { "LoyaltyDataProcessor.Storage.AccountKey", storageAccount.PrimaryAccountKey().AsSecret()},
                { "LoyaltyDataProcessorStorage.AccountName", _names.StorageAccount},
                { "LoyaltyDataProcessor.ServiceBus.ConnectionString", serviceBusConnectionString}
            };

            var octopus = new OctopusDeployment(_env);
            octopus.CreateVariableSet("Komplett.LoyaltyDataProcessor", OctopusSpace.Spaces43, thisProjectVariableSet);
            octopus.CreateVariableSet("Komplett.LoyaltyDataProcessor", OctopusSpace.Spaces1, thisProjectVariableSet);
        }

        private void CreateClubResources()
        {
            var resourceGroup = new ResourceGroup(_clubDomainNames.ResourceGroup, new ResourceGroupArgs
            {
                ResourceGroupName = _clubDomainNames.ResourceGroup,
                Location = Locations.NorwayEast,
            });
            
            var serviceBus = resourceGroup.CreateServiceBus(_clubDomainNames);
            
            serviceBus.CreteTopic("api/komplett-club/commands/join-club", resourceGroup)
                .AddSubscription("Komplett.LoyaltyProgram.Api.Join", resourceGroup);

            serviceBus.CreteTopic("api/komplett-club/commands/leave-club", resourceGroup)
            .AddSubscription("Komplett.LoyaltyProgram.Api.Leave", resourceGroup);
            
            var thisProjectVariableSet  = new OctopusVariablesMap
            {
                { "Club.ServiceBus.ConnectionString",  serviceBus.GetPrimaryConnectionString(_clubDomainNames)}
            };

            var octopus = new OctopusDeployment(_env);
            octopus.CreateVariableSet("Komplett.Club.Commands", OctopusSpace.Spaces43, thisProjectVariableSet);
            octopus.CreateVariableSet("Komplett.Club.Commands", OctopusSpace.Spaces1, thisProjectVariableSet);
        }

        private void ConfigureStorageAccountCors(StorageAccount storageAccount, ResourceGroup resourceGroup)
        {
            var corsRule = new Pulumi.AzureNative.Storage.Inputs.CorsRuleArgs
            {
                AllowedOrigins = new[] { "*" },
                AllowedMethods = new[] { "GET", "POST", "OPTIONS", "PUT", "DELETE" },
                AllowedHeaders = new[] { "*" },
                ExposedHeaders = new[] { "*" },
                MaxAgeInSeconds = 3600
            };

            new BlobServiceProperties("blobServiceCors", new BlobServicePropertiesArgs
            {
                AccountName = storageAccount.Name,
                ResourceGroupName = resourceGroup.Name,
                BlobServicesName = "default",
                Cors = new Pulumi.AzureNative.Storage.Inputs.CorsRulesArgs
                {
                    CorsRules = new[] { corsRule }
                },

            }, new CustomResourceOptions
            {
                DependsOn = new[] { storageAccount }
            });

            new TableServiceProperties("tableServiceCors", new TableServicePropertiesArgs
            {
                AccountName = storageAccount.Name,
                ResourceGroupName = resourceGroup.Name,
                TableServiceName = "default",
                Cors = new Pulumi.AzureNative.Storage.Inputs.CorsRulesArgs
                {
                    CorsRules = new[] { corsRule }
                }
            }, new CustomResourceOptions
            {
                DependsOn = new[] { storageAccount }
            });
        }

        private Component CreateAppInsights(ResourceGroup resourceGroup)
        {
            return new Pulumi.AzureNative.Insights.Component(_names.AppInsights, new()
            {
                ApplicationType = "web",
                Kind = "web",
                Location = resourceGroup.Location,
                ResourceGroupName = resourceGroup.Name,
                ResourceName = _names.AppInsights,
                RequestSource = "rest",
            });
        }

        private AppServicePlan CreateAppServicePlan(ResourceGroup resourceGroup)
        {
            return new AppServicePlan(_names.AppServicePlan, new AppServicePlanArgs
            {
                Name = _names.AppServicePlan,
                ResourceGroupName = resourceGroup.Name,
                Location = resourceGroup.Location,
                Kind = "functionapp",
                Reserved = true,
                Sku = new SkuDescriptionArgs
                {
                    Name = "Y1",
                    Size = "Y1",
                    Tier = "Dynamic",
                },
            });
        }

        private WebApp CreateWebApp(Config config, ResourceGroup resourceGroup, AppServicePlan appServicePlan, Output<string> serviceBusConnectionString, Component appInsights, StorageAccount storageAccount)
        {
            return new WebApp(_names.FunctionApp, new WebAppArgs
            {
                Name = _names.FunctionApp,
                ResourceGroupName = resourceGroup.Name,
                Location = resourceGroup.Location,
                Kind = "functionapp,linux",
                ServerFarmId = appServicePlan.Id,
                SiteConfig = new Pulumi.AzureNative.Web.Inputs.SiteConfigArgs
                {
                    NetFrameworkVersion = "v8.0",
                    AppSettings = new List<NameValuePairArgs>
                    {
                        new() { Name = "FUNCTIONS_EXTENSION_VERSION", Value = "~4" },
                        new() { Name = "FUNCTIONS_WORKER_RUNTIME", Value = "dotnet-isolated" },
                        new() { Name = "netFrameworkVersion", Value= "v8.0" },
                        new() { Name = "AzureWebJobsStorage", Value = storageAccount.PrimaryConnectionString() },

                        new() { Name = "LogsEventhubConnectionString", Value = config.Require("LogsEventhubConnectionString") },
                        new() { Name = "DspOrderChangedEventConnectionString", Value = config.Require("OrderChangedEventConnectionString") },
                        new() { Name = "LoyaltyServiceBus", Value = serviceBusConnectionString },
                        new() { Name = "StoresWithClubActivated", Value = config.Require("StoresWithClubActivated") },
                        new() { Name = "B2BStores", Value = config.Require("B2BStores") },
                        new() { Name = "LoyaltyDataProcessorStorage", Value = storageAccount.PrimaryConnectionString() },

                        new() { Name = "SCALE_CONTROLLER_LOGGING_ENABLED", Value= "AppInsights:Warning" },
                        new() { Name = "APPINSIGHTS_INSTRUMENTATIONKEY", Value= appInsights.InstrumentationKey },
                        new() { Name = "WEBSITE_MAX_DYNAMIC_APPLICATION_SCALE_OUT", Value= "1" },
                    },
                    LinuxFxVersion = "DOTNET-ISOLATED|8.0",
                    Cors = new CorsSettingsArgs
                    {
                        SupportCredentials = true,
                        AllowedOrigins = new InputList<string>
                        {
                            "https://localhost:7295"
                        }
                    }
                }
            },
            options: new CustomResourceOptions()
            {
                DependsOn = new() { appServicePlan, appInsights, storageAccount }
            });
        }
    }
}