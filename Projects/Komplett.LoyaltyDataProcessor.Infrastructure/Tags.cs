namespace Komplett.LoyaltyDataProcessor.Infrastructure;

using Pulumi;

public static class Tags
{
    public static InputMap<string> GetTags()
    {
        return new InputMap<string>
        {
            { "TechnicalOwner", "GreenSquad" },
            { "Team", "GreenSquad" },
            { "Domain", "Pricing" },
        };
    }
}