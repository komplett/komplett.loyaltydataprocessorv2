﻿namespace Komplett.LoyaltyDataProcessorV2.Function.Models;

using System.Runtime.Serialization;
using System.Text.Json;
using Azure;
using Azure.Data.Tables;

public sealed class CalendarClubPeriodEntity : ITableEntity
{
    public CalendarClubPeriodEntity()
    {
    }

    public CalendarClubPeriodEntity(string storeId, string campaignId, decimal pointsMultiplier, string description, DateTime startDate, DateTime endDate, DateTime campaignStartDate, DateTime campaignEndDate, DateTime? specificDate = null, IEnumerable<string> selectedDaysOfWeek = null)
    {
        PartitionKey = storeId;
        RowKey = specificDate.HasValue
            ? $"{campaignId}_{specificDate:yyyyMMdd}"
            : $"{campaignId}_{startDate:yyyyMMdd}_{endDate:yyyyMMdd}";
        StoreId = storeId;
        CampaignId = campaignId;
        StartDate = startDate;
        EndDate = endDate;
        SpecificDate = specificDate;
        PointsMultiplier = (double)pointsMultiplier;
        Description = description;
        DaysOfWeek = selectedDaysOfWeek;
        CampaignEndDate = campaignEndDate;
        CampaignStartDate = campaignStartDate;
    }

    public string StoreId { get; set; }

    public string CampaignId { get; set; }

    public DateTime StartDate { get; set; }

    public DateTime EndDate { get; set; }

    public DateTime CampaignStartDate { get; set; }

    public DateTime CampaignEndDate { get; set; }

    public DateTime? SpecificDate { get; set; }

    public double PointsMultiplier { get; set; }

    public string Description { get; set; }

    public string DaysOfWeekJson { get; set; }

    [IgnoreDataMember]
    public IEnumerable<string> DaysOfWeek
    {
        get => string.IsNullOrEmpty(DaysOfWeekJson)
            ? null
            : JsonSerializer.Deserialize<IEnumerable<string>>(DaysOfWeekJson);

        set => DaysOfWeekJson = value == null
            ? null
            : JsonSerializer.Serialize(value);
    }

    public string PartitionKey { get; set; }

    public string RowKey { get; set; }

    public DateTimeOffset? Timestamp { get; set; }

    public ETag ETag { get; set; }
}