namespace Komplett.LoyaltyDataProcessorV2.Function.Orders.Dsp;

public class OrderCancelled
{
    public const string TypeName = "Komplett.Orders.DSP.Processor.Contract.Outbound.OrderCancelled";

    public string OrderId { get; set; }
}