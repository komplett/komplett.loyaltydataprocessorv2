namespace Komplett.LoyaltyDataProcessorV2.Function.Orders.Dsp;

public class InvoiceLine
{
    public int Quantity { get; set; }

    public Price UnitPrice { get; set; }
}