namespace Komplett.LoyaltyDataProcessorV2.Function.Orders.Dsp;

public record OrderLine
{
    public string MaterialNumber { get; set; }
}