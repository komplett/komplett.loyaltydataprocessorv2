namespace Komplett.LoyaltyDataProcessorV2.Function.Orders.Dsp;

public class OrderPlaced
{
    public const string TypeName = "Komplett.Orders.DSP.Processor.Contract.Outbound.OrderPlaced";

    public string OrderId { get; set; }

    public string SoldToCustomerId { get; set; }

    public string StoreId { get; set; }

    public DateTime PlacedAt { get; set; }

    public OrderLine[] OrderLines { get; set; }
}