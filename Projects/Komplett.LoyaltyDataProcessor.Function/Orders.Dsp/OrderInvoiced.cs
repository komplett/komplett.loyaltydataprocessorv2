namespace Komplett.LoyaltyDataProcessorV2.Function.Orders.Dsp;

public class OrderInvoiced
{
    public const string TypeName = "Komplett.Orders.DSP.Processor.Contract.Outbound.OrderInvoiced";

    public string OrderId { get; set; }

    public InvoiceLine[] InvoiceLines { get; set; }
}