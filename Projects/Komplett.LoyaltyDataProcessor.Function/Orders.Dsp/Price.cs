namespace Komplett.LoyaltyDataProcessorV2.Function.Orders.Dsp;

public class Price
{
    public decimal Gross { get; set; }

    public decimal Net { get; set; }
}