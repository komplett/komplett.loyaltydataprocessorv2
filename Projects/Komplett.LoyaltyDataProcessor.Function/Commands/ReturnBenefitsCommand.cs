namespace Komplett.LoyaltyDataProcessorV2.Function.Commands;

using System.Collections.Generic;

public class ReturnBenefitsCommand
{
    public string CustomerId { get; set; }

    public string StoreId { get; set; }

    public string OrderId { get; set; }

    public IEnumerable<string> MaterialNumbers { get; set; }
}