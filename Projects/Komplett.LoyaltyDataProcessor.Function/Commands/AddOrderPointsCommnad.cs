﻿namespace Komplett.LoyaltyDataProcessorV2.Function.Commands;

public record AddOrderPointsCommnad(string CustomerId, decimal Points, string StoreId, DateTime When);