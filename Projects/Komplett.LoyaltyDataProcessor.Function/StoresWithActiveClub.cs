﻿namespace Komplett.LoyaltyDataProcessorV2.Function;

using System;
using System.Linq;

public static class StoresWithActiveClub
{
    public static bool IsDisabled(string storeId)
    {
        var storesWithActiveClub = Environment.GetEnvironmentVariable("StoresWithClubActivated")?.Split(",") ?? Array.Empty<string>();
        return !storesWithActiveClub.Contains(storeId);
    }

    public static bool IsB2BStore(string storeId)
    {
        return Environment.GetEnvironmentVariable("B2BStores") !.Contains(storeId);
    }
}