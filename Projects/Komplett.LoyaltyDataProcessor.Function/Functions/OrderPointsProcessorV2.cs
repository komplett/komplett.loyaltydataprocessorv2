namespace Komplett.LoyaltyDataProcessorV2.Function.Functions;

using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Azure.Storage.Blobs;
using Komplett.LoyaltyDataProcessorV2.Function.Commands;
using Komplett.LoyaltyDataProcessorV2.Function.Orders.Dsp;
using Microsoft.Azure.Functions.Worker;
using Newtonsoft.Json;
using Services;

public class OrderPointsProcessorV2(ServiceBusClient serviceBusClient, ILoyaltyPointMultiplierClient loyaltyPointMultiplierClient)
{
    [Function("OrderPointsProcessorV2")]
    public async Task Run(
        [ServiceBusTrigger(
            "api/komplett.orders.dsp.processor/events/order-changed",
            "komplett-komplett.loyaltydataprocessor-v2",
            Connection = "DspOrderChangedEventConnectionString",
            IsSessionsEnabled = true)] ServiceBusReceivedMessage inputMessage,
        [BlobInput("orders", Connection = "AzureWebJobsStorage")] BlobContainerClient blobContainer)
    {
        var orderEventType = GetTypename(inputMessage);

        switch (orderEventType)
        {
            case OrderPlaced.TypeName:
                {
                    var orderPlaced = JsonConvert.DeserializeObject<OrderPlaced>(Encoding.UTF8.GetString(inputMessage.Body));

                    if (StoresWithActiveClub.IsDisabled(orderPlaced.StoreId))
                    {
                        return;
                    }

                    var blob = blobContainer.GetBlobClient($"{orderPlaced.OrderId}.json");
                    await blob.UploadAsync(inputMessage.Body, overwrite: true);

                    return;
                }

            case OrderInvoiced.TypeName:
                {
                    var orderInvoiced = JsonConvert.DeserializeObject<OrderInvoiced>(Encoding.UTF8.GetString(inputMessage.Body));

                    var orderPlacedBlob = blobContainer.GetBlobClient($"{orderInvoiced.OrderId}.json");

                    var blobJson = await GetBlob(orderPlacedBlob);

                    if (blobJson is null)
                    {
                        return;
                    }

                    var orderPlaced = JsonConvert.DeserializeObject<OrderPlaced>(blobJson);

                    var multiplier = loyaltyPointMultiplierClient.GetMultiplier(orderPlaced.StoreId, orderPlaced.PlacedAt);

                    var totalPointsEarned = PointsCalculator.Sum(orderPlaced.StoreId, orderInvoiced.InvoiceLines);
                    var points = totalPointsEarned * multiplier;
                    var command = new AddOrderPointsCommnad(orderPlaced.SoldToCustomerId, points, orderPlaced.StoreId, orderPlaced.PlacedAt);

                    var commandJson = JsonConvert.SerializeObject(command);
                    var message = new ServiceBusMessage(Encoding.UTF8.GetBytes(commandJson))
                    {
                        SessionId = orderInvoiced.OrderId,
                        Subject = nameof(AddOrderPointsCommnad)
                    };

                    var blob = blobContainer.GetBlobClient($"add-points-cmd-{orderPlaced.OrderId}-{DateTime.Now:yyyyMMddTHHmmss}.json");

                    await blob.UploadAsync(BinaryData.FromString(JsonConvert.SerializeObject(commandJson)));
                    await SendMessageToQueue("add-points", message);

                    break;
                }

            case OrderCancelled.TypeName:
                {
                    var orderCancelled = JsonConvert.DeserializeObject<OrderCancelled>(Encoding.UTF8.GetString(inputMessage.Body));

                    var orderPlacedBlob = blobContainer.GetBlobClient($"{orderCancelled.OrderId}.json");

                    var blobJson = await GetBlob(orderPlacedBlob);

                    if (blobJson is null)
                    {
                        return;
                    }

                    var orderPlaced = JsonConvert.DeserializeObject<OrderPlaced>(blobJson);

                    var command = new ReturnBenefitsCommand
                    {
                        CustomerId = orderPlaced.SoldToCustomerId,
                        OrderId = orderPlaced.OrderId,
                        StoreId = orderPlaced.StoreId,
                        MaterialNumbers = orderPlaced.OrderLines.Select(p => p.MaterialNumber)
                    };

                    var commandJson = JsonConvert.SerializeObject(command);
                    var message = new ServiceBusMessage(Encoding.UTF8.GetBytes(commandJson))
                    {
                        SessionId = $"{@command.CustomerId}-{orderCancelled.OrderId}",
                        Subject = nameof(ReturnBenefitsCommand)
                    };

                    await SendMessageToQueue("return-benefits-order-canceled", message);

                    var blob = blobContainer.GetBlobClient($"{orderPlaced.OrderId}-return-benefits-cmd.json");
                    await blob.UploadAsync(BinaryData.FromString(JsonConvert.SerializeObject(commandJson)));

                    var blobWithOrderToRemove = blobContainer.GetBlobClient($"{orderPlaced.OrderId}.json");
                    await blobWithOrderToRemove.DeleteIfExistsAsync();

                    break;
                }

            default:
                {
                    return;
                }
        }
    }

    private static string GetTypename(ServiceBusReceivedMessage message)
    {
        var exists = message.ApplicationProperties.TryGetValue("x-typename", out object value);

        return exists ? value.ToString() : string.Empty;
    }

    private async ValueTask<string> GetBlob(BlobClient blobClient)
    {
        string blobJson;

        try
        {
            var response = await blobClient.DownloadAsync();
            using var reader = new StreamReader(response.Value.Content);
            blobJson = await reader.ReadToEndAsync();
        }
        catch (Azure.RequestFailedException)
        {
            // Order invoiced might be resent, it means the original blob with the order is no longer here since it was renamed after sending points.
            return null;
        }

        if (string.IsNullOrWhiteSpace(blobJson))
        {
            // order invocied might be resend, it means original blob with order is no longer here since it was renamed after sending points
            return null;
        }

        return blobJson;
    }

    private async Task SendMessageToQueue(string queueName, ServiceBusMessage message)
    {
        await using var sender = serviceBusClient.CreateSender(queueName);

        message.ContentType = "application/json";

        await sender.SendMessageAsync(message);
    }
}