﻿using Azure.Messaging.ServiceBus;
using Komplett.Logging.Microsoft.Eventhub;
using Komplett.LoyaltyDataProcessorV2.Function.Constants;
using Komplett.LoyaltyDataProcessorV2.Function.Services;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

var host = new HostBuilder()
           .ConfigureFunctionsWebApplication().
           ConfigureAppConfiguration(configurationBuilder =>
           {
               configurationBuilder.AddEnvironmentVariables();
           })
           .ConfigureServices((context, services) =>
           {
               var connectionString = context.Configuration["LoyaltyServiceBus"];
               services.AddSingleton(_ => new ServiceBusClient(connectionString));
               services.AddAzureClients(b =>
               {
                   b.AddTableServiceClient(context.Configuration[Constants.LoyaltyDataProcessorStorageConnectionString])
                       .WithName(nameof(LoyaltyPointMultiplierClient));
               });
               services.AddScoped<ILoyaltyPointMultiplierClient, LoyaltyPointMultiplierClient>();
               services.AddLogging(
                   config =>
                   {
                       config.ClearProviders();

                       config.SetMinimumLevel(LogLevel.Warning);
                       if (context.HostingEnvironment.IsDevelopment())
                       {
                           config.AddConsole().AddDebug();
                       }
                       else
                       {
                           config.AddEventHubLogger(
                               configuration =>
                               {
                                   configuration.Owner = "Green";
                                   configuration.ApplicationName = "func-komplett-loylaty-data-processor";
                                   configuration.EventHubConnectionString =
                                       Environment.GetEnvironmentVariable(
                                           "LogsEventhubConnectionString");
                               });
                       }
                   });
           }).Build();

await host.RunAsync();