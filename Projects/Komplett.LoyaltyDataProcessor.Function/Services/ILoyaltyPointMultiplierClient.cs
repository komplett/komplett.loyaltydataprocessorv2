﻿namespace Komplett.LoyaltyDataProcessorV2.Function.Services;

using Azure.Data.Tables;
using Constants;
using Microsoft.Extensions.Azure;
using Models;

public interface ILoyaltyPointMultiplierClient
{
    decimal GetMultiplier(string storeId, DateTime orderDate);
}

public sealed class LoyaltyPointMultiplierClient : ILoyaltyPointMultiplierClient
{
    private readonly TableClient _tableClient;

    public LoyaltyPointMultiplierClient(IAzureClientFactory<TableServiceClient> clientFactory)
    {
        var tableServiceClient = clientFactory.CreateClient(nameof(LoyaltyPointMultiplierClient));
        _tableClient = tableServiceClient.GetTableClient(Constants.LoyaltyPointsMultiplierDatesTableName);
    }

    public decimal GetMultiplier(string storeId, DateTime orderDate)
    {
        var utcStart = orderDate.Date.ToUniversalTime();
        var utcEnd = orderDate.Date.ToUniversalTime();

        var result = _tableClient.Query<CalendarClubPeriodEntity>(
            c =>
                c.PartitionKey == storeId &&
                (
                    (c.SpecificDate.HasValue &&
                     c.SpecificDate.Value >= utcStart &&
                     c.SpecificDate.Value <= utcEnd) ||
                    (!c.SpecificDate.HasValue &&
                     c.StartDate <= utcEnd &&
                     c.EndDate >= utcStart)
                )).ToList();

        var pointsMultiplier = result.FirstOrDefault()?.PointsMultiplier is { } multiplier
            ? (decimal)multiplier
            : 1m;

        return pointsMultiplier;
    }
}