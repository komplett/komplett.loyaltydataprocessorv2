﻿namespace Komplett.LoyaltyDataProcessorV2.Function.Constants;

public static class Constants
{
    public const string LoyaltyPointsMultiplierDatesTableName = "LoyaltyPointMultiplierDates";

    public const string LoyaltyDataProcessorStorageConnectionString = "LoyaltyDataProcessorStorage";
}