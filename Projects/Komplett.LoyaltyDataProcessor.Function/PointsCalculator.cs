﻿namespace Komplett.LoyaltyDataProcessorV2.Function;

using System.Linq;
using Orders.Dsp;

public static class PointsCalculator
{
    public static decimal Sum(string storeId, InvoiceLine[] invoiceLines)
    {
        return StoresWithActiveClub.IsB2BStore(storeId)
            ? invoiceLines.Sum(p => p.UnitPrice.Net * p.Quantity)
            : invoiceLines.Sum(p => p.UnitPrice.Gross * p.Quantity);
    }
}