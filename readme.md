# Orders Loyalty integration

---

### Architecture:

- [Diagram](docs/OrderLoyaltyClubIntegration.drawio.png)

---

### What is a order and loyalty integration for?

When customer orders goods from store we add loyalty points to his account. Points are equal order total gross value.

High level overview:
Order placed events is giving info about placed order with all lines and values.
When order is invocied we send command to Komplett.Loyalty.DataPricessor and  later to Komplett.LoyaltyProgram.Api to add points to account.
