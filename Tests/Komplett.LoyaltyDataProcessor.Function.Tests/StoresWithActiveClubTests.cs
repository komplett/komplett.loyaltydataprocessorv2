using NUnit.Framework;

namespace Komplett.LoyaltyDataProcessor.Function;

using System;
using LoyaltyDataProcessorV2.Function;

[TestFixture]
public class StoresWithActiveClubTests
{
    [Test]
    public void IsDisabled_StoreIdNotInStoresWithActiveClub_ReturnsTrue()
    {
        // Arrange
        var storeId = "324";
        var activeStores = "312,310"; // Assuming "123" is not in this list
        Environment.SetEnvironmentVariable("StoresWithClubActivated", activeStores);
        
        // Act
        var result = StoresWithActiveClub.IsDisabled(storeId);
        
        // Assert
        Assert.IsTrue(result);
    }

    [Test]
    public void IsDisabled_StoreIdInStoresWithActiveClub_ReturnsFalse()
    {
        // Arrange
        var storeId = "310";
        var activeStores = "310,312"; // Assuming "456" is in this list
        Environment.SetEnvironmentVariable("StoresWithClubActivated", activeStores);

        // Act
        var result = StoresWithActiveClub.IsDisabled(storeId);

        // Assert
        Assert.IsFalse(result);
    }

    [Test]
    public void IsDisabled_StoresWithActiveClubIsNull_ReturnsTrue()
    {
        // Arrange
        var storeId = "310";
        Environment.SetEnvironmentVariable("StoresWithClubActivated", null);

        // Act
        var result = StoresWithActiveClub.IsDisabled(storeId);

        // Assert
        Assert.IsTrue(result);
    }

    [Test]
    public void IsDisabled_StoreIdNotInStoresWithActiveClub_EmptyString_ReturnsTrue()
    {
        // Arrange
        var storeId = "310";
        Environment.SetEnvironmentVariable("StoresWithClubActivated", "");

        // Act
        var result = StoresWithActiveClub.IsDisabled(storeId);

        // Assert
        Assert.IsTrue(result);
    }

    [Test]
    public void IsDisabled_StoreIdInStoresWithActiveClub_EmptyString_ReturnsFalse()
    {
        // Arrange
        var storeId = "310";
        Environment.SetEnvironmentVariable("StoresWithClubActivated", "310,");

        // Act
        var result = StoresWithActiveClub.IsDisabled(storeId);

        // Assert
        Assert.IsFalse(result);
    }
}