namespace Komplett.LoyaltyDataProcessor.Function;

using System;
using System.Linq;
using LoyaltyDataProcessorV2.Function;
using LoyaltyDataProcessorV2.Function.Orders.Dsp;
using NUnit.Framework;

[TestFixture]
public class PointsCalculatorTests
{
    [Test]
    public void PointsCalculator_ForB2BStore_CalculatesNetLineValue()
    {
        // Arrange
        var storeId = "324";
        Environment.SetEnvironmentVariable("B2BStores", "324,344");
        
        // Act
        var lines = new[]
        {
            new InvoiceLine
            {
                Quantity = 1,
                UnitPrice = new Price
                {
                    Gross = 250,
                    Net = 200
                }
            },
            new InvoiceLine
            {
                Quantity = 2,
                UnitPrice = new Price
                {
                    Gross = 125,
                    Net = 100
                }
            }
        };
        
        var result = PointsCalculator.Sum(storeId, lines);
        
        // Assert
        Assert.AreEqual(lines.Sum(p => p.UnitPrice.Net * p.Quantity), result);
    }
    
    [Test]
    public void PointsCalculator_ForB2CStore_CalculatesGrossLineValue()
    {
        // Arrange
        var storeId = "310";
        Environment.SetEnvironmentVariable("B2BStores", "324,344");
        
        // Act
        var lines = new[]
        {
            new InvoiceLine
            {
                Quantity = 1,
                UnitPrice = new Price
                {
                    Gross = 250,
                    Net = 200
                }
            },
            new InvoiceLine
            {
                Quantity = 2,
                UnitPrice = new Price
                {
                    Gross = 125,
                    Net = 100
                }
            }
        };
        
        var result = PointsCalculator.Sum(storeId, lines);
        
        // Assert
        Assert.AreEqual(lines.Sum(p => p.UnitPrice.Gross * p.Quantity), result);
    }
}