param (
    [string]$Version = ""
 )

#Set-PSDebug -Trace 1

# Stop the build if any step fails
$ErrorActionPreference = "Stop"

$ScriptPath = split-path -parent $MyInvocation.MyCommand.Definition;
$RootPath = (Get-Item -Path "$ScriptPath\.." -Verbose).FullName
$Solution = Get-ChildItem "$RootPath\*.sln";
$Name = $Solution.BaseName;
$ReleaseDir = "$RootPath\Release";
$ReportsDir = "$ReleaseDir\Reports";
$OctopusPackagesDir = "$RootPath\Release\OctopusPackages";

Function Step
{
    [CmdletBinding()]
    param (
        [Parameter(Position=0, Mandatory=1)]
		[string]$Description,
        [Parameter(Position=1, Mandatory=1)]
        [scriptblock]$Command
    )

	Write-Host `n------------[$Description]------------

	Write-Host $Command.ToString();

    & $Command
    if ($LastExitCode -ne 0) {
        throw "Execution of command:`n  $Command`nFailed with exitcode $LastExitCode"
    }

	Write-Host `n------------[/$Description]------------
}

# Clean up release output
Remove-Item -Path "$ReleaseDir" -ErrorAction Ignore -Recurse
New-Item -ItemType Directory -Path "$ReleaseDir" -Force | Out-Null
New-Item -ItemType Directory -Path "$ReportsDir" -Force | Out-Null
New-Item -ItemType Directory -Path "$OctopusPackagesDir" -Force | Out-Null

Write-Host Building version $Version of $Name

Step "Restore solution" {
	dotnet restore $Solution "/p:Version=$Version" --configfile "$RootPath\NuGet.Config" --no-cache
}

Step "Clean solution" {
	dotnet clean $Solution -c Release
}

Step "Build solution" {
	dotnet build $Solution -c Release "/p:Version=$Version" --no-restore
}

Step "Publish solution" {
	dotnet publish $Solution -c Release "/p:Version=$Version;OctopusDir=$OctopusPackagesDir" --no-restore
}