param (
    [string]$nugetServer = "",
    [string]$nugetApiKey = "",
    [string]$octopusServer = "",
    [string]$octopusApiKey = "",
    [string]$jiraUsername = "",
    [string]$jiraPassword = ""
 )

#Set-PSDebug -Trace 1

# Stop the build if any step fails
$ErrorActionPreference = "Stop"

$ScriptPath = split-path -parent $MyInvocation.MyCommand.Definition;
$RootPath = (Get-Item -Path "$ScriptPath\.." -Verbose).FullName
$Solution = Get-ChildItem "$RootPath\*.sln";
$Name = $Solution.BaseName;
$ReleaseDir = "$RootPath\Release";
$ReportsDir = "$ReleaseDir\Reports";
$PublishDir = "$ReleaseDir\Publish";
$OctopusPackagesDir = "$RootPath\Release\OctopusPackages";
$NugetPackagesDir = "$RootPath\Release\NugetPackages";

Function Step
{
    [CmdletBinding()]
    param (
        [Parameter(Position=0, Mandatory=1)]
		[string]$Description,
        [Parameter(Position=1, Mandatory=1)]
        [scriptblock]$Command
    )

	Write-Host `n------------[$Description]------------

	Write-Host $Command.ToString();

    & $Command
    if ($LastExitCode -ne 0) {
        throw "Execution of command:`n  $Command`nFailed with exitcode $LastExitCode"
    }

	Write-Host `n------------[/$Description]------------
}

# Clean up release output
Remove-Item -Path "$ReleaseDir" -ErrorAction Ignore -Recurse
New-Item -ItemType Directory -Path "$ReleaseDir" -Force | Out-Null
New-Item -ItemType Directory -Path "$ReportsDir" -Force | Out-Null
New-Item -ItemType Directory -Path "$OctopusPackagesDir" -Force | Out-Null

$Version = Step "Get build version" {
	komplett-build next-version
}

Write-Host Publishing version $Version of $Name

Step "Build" {
    ./build.ps1 -Version $Version
}

Get-ChildItem "$OctopusPackagesDir\*" -Directory | ForEach-Object {
	Step "Package $($_.BaseName)" {
		komplett-build octo-pack --source-dir=$($_.FullName) --id=$($_.BaseName) --version=$Version --destination-dir=$OctopusPackagesDir --verbose
	}
}

Step "Update Jira" {
	komplett-build update-jira -u "$jiraUsername" -p "$jiraPassword" -c "$Name"
}

Step "Create tag v$Version" {
	git tag -a "v$Version" -m "Release $Version created by Jenkins"
}

Step "Push tag" {
	git push origin "v$Version" --porcelain
}

Get-ChildItem "$OctopusPackagesDir\*.zip" -File | Sort-Object { $_.Name -Like '*DbUp*' } | ForEach-Object {
	Step "Push $($_.BaseName).zip" {
		komplett-build octo-push --package=$($_.FullName) --server=$octopusServer --api-key=$octopusApiKey --verbose --spaceId="Spaces-43"
	}
}
